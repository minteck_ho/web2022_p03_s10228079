﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Web_S10228079.Models
{
    public class Staff
    {
        [Display(Name = "ID")]
        public int StaffId { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "cannot exceed 50 characters")]
        public string Name { get; set; }

        public char Gender { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        public string Nationality { get; set; }

        [Display(Name="Email Address")]
        [EmailAddress] // Validation Annotation for email address format
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get; set; }

        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00, ErrorMessage =
        "Invalid value! Salary must be between 1.00 to 10000.00")]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }
        
        [Display(Name="Branch")]
        public int? BranchNo { get; set; }
        
    }
}
